Co-operatives and how they solve problems with Principle – Agent Theory.
Kevin Morris, Data Analytics Leader, Canadian Credit Union Association

One thing I love about co-operatives is that their organizational structure by design attempts to solve a major negative societal result of something called the principle-agent problem. Principle Agent theory essentially argues that organizations have a Principle (in for-profit corporations this is usually the investor or stock holder), and the Agent (the CEO). In your typical anglo-saxon capitalist corporation, the Principle’s direction to their Agent is to make them a profit. That Agent then makes decisions in order to achieve the Principle’s goal.









In some cases, there is a disconnect between the Agent and the Principle. In for-profit corporations, the principle usually has too many investments to care about anything except for profit. For example, mutual funds usually hold hundreds of stocks; an investor isn’t concerned about the practices of each company but that they are creating a good return on their investment. The Agent is often in a bind; they might want to be more sustainable, provide better services, or strategically invest in their employees more, but their Principle has given them a direction to attain short-term profit maximization. There is no greedy individual at fault here, it’s simply through multiple layers of disconnect that the Agent is forced to act in a way that serves the direction of the Principle. When you look at this at a macro scale you see a system of capitalism in which no one person individually is at fault, but that the world is filled with inequalities and corporations behaving badly in order to achieve high profits.





There are two common solutions that societies implement; One solution is for the government to regulate profit seeking organizations to limit how they operate (for example, stopping a company from making money by disposing of it’s toxic waste in a river). This will continue to be a solution as long as we have governments and private enterprise, but sometimes governments are susceptible to make decisions based on incomplete theories or inaccurate data. This sometimes hurts the organization (including co-operatives) from operating successfully, and also the public by making it harder for them to purchase from the organization. There is also the thought that investors can invest in Socially Responsible Investments (SRIs), so that the Principle will set conditions beyond profit for the Agent to achieve, thereby decreasing the negative repercussions of profit maximization. This has had varying levels of success because of the small proportion that SRIs have in relation to profit maximizing funds. For example Larry Fink, the CEO of BlackRock Asset Management (the largest asset management firm in the world), just recently stated that they would not actively vote for socially responsible decisions in corporate proxy votes, but only have backroom conversations to suggest improvement. (source: http://www.institutionalinvestor.com/article/3590250/asset-management-hedge-funds-and-alternatives/blackrock-ceo-larry-fink-champions-long-term-value-creation.html )

Notice how in the model above, customers have no say in what decisions are being made. The Principle and the Agent are only concerned about profit for the Principle (Investor). With co-operatives, the Principle are the members of the co-operative. The Agent (CEO) is directed by their principle, and the interests of the principle are usually the product or service the co-operative provides, and not profit. Organizational efficiencies aside, this leads to a positive feedback loop where the Agent makes decisions in the best interests of the Principle (the members), which is one of the reasons co-operatives are known to be more sustainable organizations with better client service. Which is one thing I love about co-operatives.


